#!/usr/bin/env python2
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.
#

import fountain
import glass

SSID_MAX_SIZE = 32

totransmit = open('../sandbox/ssid.txt.cpabe.base64')
m = ''.join(totransmit.readlines())

f = fountain.Fountain(m, chunk_size = SSID_MAX_SIZE)
g = glass.Glass(f.num_chunks)

i = 0
while not g.isDone():
    i += 1
    d = f.droplet()
    g.addDroplet(d)

print g.getString()
print i

