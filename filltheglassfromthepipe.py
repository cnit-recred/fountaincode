#!/usr/bin/env python2
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.
#

import glass
import droplet
import base64
import sys

sys.setrecursionlimit(10000)

#print m
#sys.exit(0)

INPIPENAME = '/tmp/ssidpipe2'
OUTFILENAME = '/tmp/wifiabe.cpabe.base64'

inpipe = open(INPIPENAME, 'r')
outfile = open(OUTFILENAME, 'w')

g = None

recset = set()

i = 0
j = 0
while not g or not g.isDone():
    vendorelems = inpipe.readline()
    vendorelems = vendorelems.strip()
    print "[%d] %s" % (i, vendorelems)
    if g:
        print "[%d / %d]" % (g.chunksDone(), num_chunks),
    if vendorelems in recset:
        print "dejavu!"
        j += 1
        continue
    recset.add(vendorelems)
    try:
        elemslist = vendorelems.split(' ')
        num_chunks = int(elemslist[0], 16)
        seed = int("".join(elemslist[1:5]), 16)
        data = ''.join([chr(int(e, 16)) for e in elemslist[5:]])
        #print data
    except Exception, e:
        print "error:" + str(e)
        continue
        #raise
    #decodeddata = base64.b64decode(data)
    decodeddata = data
    print "num_chunks: %d, seed: %d, datalen: %d" % (num_chunks, seed, len(decodeddata))
    d = droplet.Droplet(decodeddata, seed, num_chunks)
    if not g:
        g = glass.Glass(num_chunks)
    g.addDroplet(d)
    i += 1

print "%d different drops and %d repeated drops received for %d chunks" % (i, j, num_chunks)

outfile.write(g.getString().rstrip('X'))
print "output written to ", OUTFILENAME


