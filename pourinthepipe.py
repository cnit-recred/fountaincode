#!/usr/bin/env python2
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.
#

import fountain
import base64
import sys

#SSID_MAX_SIZE = 254
#SSID_TARGET_SIZE = 230
SSID_MAX_SIZE = 514 # 252 bytes of payload
#SSID_TARGET_SIZE = 183
SSID_TARGET_SIZE = 247

totransmit = open('/home/clauz/netgroup/recred/cpabe/cpabe-0.11/sandbox/ssid.txt.cpabe.base64')

m = ''.join([line.strip() for line in totransmit.readlines()])

#print m
#sys.exit(0)

outpipe = open('/tmp/ssidpipe', 'w')

f = fountain.Fountain(m, chunk_size = SSID_TARGET_SIZE)

i = 0
while True:
    d = f.droplet()
    #encodeddata = base64.b64encode(d.data)
    encodeddata = d.data

    # 3 bytes for the OUI, 5 bytes for num_chunks and seed
    houts =  "dd%02x000c42" % (3 + 5 + len(encodeddata),) 
    houts += "%02x%08x" % (d.num_chunks, d.seed)
    houts += "".join(["%02x" % ord(c) for c in encodeddata])
    assert len(houts) % 2 == 0

    while len(houts) < SSID_MAX_SIZE:
        # padding
        houts += "."

    assert len(houts) == SSID_MAX_SIZE 
    
    #outpipe.write(outs)
    outpipe.write(houts)
    print "[%d] %s [%d] [%d ; %d]" % (i, houts, len(houts), d.seed, d.num_chunks)
    i += 1


